# Shell commands to build and run Ikb Odoo Docker 

## Build 
This must be ran in the appserver / workspace folder

    docker build --pull --no-cache --rm -f ".devcontainer/Dockerfile" -t inouk.ikb16c:latest .   
    docker build --pull            --rm -f ".devcontainer/Dockerfile" -t inouk.ikb16c:latest .   
                                                                                    context--^

## Images Registry Storage

ikb images are stored in Gitlab Registry.
Images are public.

### Push
A Gitlab Personal Access Token is required to push images.
See: https://docs.gitlab.com/ee/user/packages/container_registry/index.html

    docker login registry.gitlab.com  # Use a PAT
    docker tag inouk.ikb16c:latest registry.gitlab.com/inouk/appserver-ikb/inouk.ikb16c:latest 
    docker push registry.gitlab.com/inouk/appserver-ikb/inouk.ikb16c:latest   

## Run

Reminder:  
- ENTRYPOINT defines the command executed when the container is launched
- CMD defines the default parameters. They are overwritten with the docker run params.

By default, the container will run the bin/start_odoo (the ENTRYPOINT) and the run args will be passed to odoo.

    # Example
    docker run -it inouk.ikb16c:latest --help

To run a shell, we must change the ENTRYPOINT:

    # launch a shell 
    docker run -it --entrypoint=/bin/bash inouk.ikb16c:latest 

To run Odoo:

    # Locally
    sudo docker run -p 8069:8069/tcp -p 8072:8072/tcp \
        --env-file=/etc/muppy.env \
        -it --rm inouk.ikb16c:latest  --workers=2

    # From registry
    sudo docker run -p 8069:8069/tcp -p 8072:8072/tcp \
        --env-file=/etc/muppy.env \
        -it --rm registry.gitlab.com/inouk/appserver-ikb/inouk.ikb16c:latest --workers=2

