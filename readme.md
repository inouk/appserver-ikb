# Odoo IKB

This is an odoo.sh compatible repository.

Odoo configuration is managed by inouk buildit aka ikb the .ikb folder.

# Launch odoo
To run odoo, use odoo-ikb which accept all odoo args and parameters.
```bash
odoo-ikb 
```

# Develop
Create or copy your addons in this directory.

# What is .devcontainer

This is under development. Please ignore it for now.